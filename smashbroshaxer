#!/bin/bash

basedir=$(readlink -m "${BASH_SOURCE[0]}")
basedir=$(dirname ${basedir})

reldate="2016/07/25"
version="3.0.1"

if [[ $(uname -m) == "x86_64" ]]; then
	YAD=${basedir}/bin/yad.64
	AIREPLAY=${basedir}/bin/aireplay-ng.64
else	YAD=${basedir}/bin/yad.32
	AIREPLAY=${basedir}/bin/aireplay-ng.32
fi

hax_my_smash () {

	xterm -e /bin/bash -c \
		"printf \">> Enter password to access WiFi device:

>> press Ctrl+C once HBL started to stop this script!

\" && \
		sudo ifconfig ${device} down && \
		sudo iwconfig ${device} mode monitor && \
		sudo ifconfig ${device} up && \
		sudo iwconfig ${device} channel 6 && \
		yes | sudo ${AIREPLAY} \
			--interactive \
			-r ${replay} \
			-h 59:ee:3f:2a:37:e0 \
			-x 20 \
			${device}"

}

reset_wifi () {

	xterm -e /bin/bash -c \
		"printf \">> Enter password to reset WiFi device:

\" && \
		sudo ifconfig ${device} down && \
		sudo iwconfig ${device} mode managed && \
		sudo ifconfig ${device} up"

}

open_3ds_homebrew () {
	xdg-open "https://smealum.github.io/3ds/"
}

open_smashbroshax () {
	xdg-open "https://github.com/yellows8/3ds_smashbroshax"
}

interactive_mode () {

	wireless=""
	for device in /sys/class/net/*; do
		[[ -e ${device}/wireless ]] && \
			wireless+="$(basename ${device}),"
	done

	pcaps=""
	for pcap in ${basedir}/pcap/*.pcap; do
		pcaps+="$(basename ${pcap}),"
	done

	wireless="${wireless%"${wireless##*[!,]}"}"
	pcaps="${pcaps%"${pcaps##*[!,]}"}"

	array=($(${YAD} \
		--title="smashbroshaxer ${version}" \
		--form \
		--item-separator=, \
		--separator=" " \
		--field="		smashbroshaxer will send data to your 3DS in order for

					»Super Smash Bros for 3DS«

		to start the Homebrew Launcher. Game version up to 1.1.2 supported;
		(if you got a newer version, (temporarily) uninstall the update data).

		Note: retail cardtriges version 1.1.0 (with amiibo logo) are known to
		not be supported by smashbroshax.
:LBL" " " \
		--field="1. Select WiFi Device to use:CB" "${wireless}" \
		--field="2. Select your Game version:CB" "${pcaps}" \
		--field="
3. Start game, click 'hax my smash', then to go Smash > Group
:LBL" " "\
		--button="! hax my smash:0" \
		--button="x cancel:1" \
		--button="? 3DS Homebrew:2" \
		--button="? smashbroshax:3"))

	ret=$?

	device=${array[0]}
	replay=${basedir}/pcap/${array[1]}

	case ${ret} in
		0 )	hax_my_smash
			reset_wifi ;;

		2 )	open_3ds_homebrew	;;

		3 )	open_smashbroshax	;;

		1 )	echo -e "\n  cancelled by user \n"
			exit 1
		;;
	esac

}

case ${1} in
	--help )
		echo -e "
smashbroshaxer version ${version} (${reldate})

usage:
	smashbroshaxer			| interactive (UI) mode
	smashbroshaxer <wifi> <pcap>	| non-interactive mode
"
	;;

	"" )
		interactive_mode
	;;

	* )
		if [[ -e /sys/class/net/${1}/wireless ]]; then
			device=${1}
		else	echo -e "\nspecified device ${1} is not a wireless device!\n"
			exit 1
		fi

		if [[ -f ${2} ]]; then
			replay=$(readlink -m ${2})
		else	echo -e "\nspecified replay ${2} does not exist!\n"
			exit 1
		fi

		hax_my_smash
		reset_wifi
	;;
esac
